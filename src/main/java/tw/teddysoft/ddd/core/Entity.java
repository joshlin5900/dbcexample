package tw.teddysoft.ddd.core;

import java.io.Serializable;

import static tw.teddysoft.dbc.Contract.requireNotNull;

public abstract class Entity<ID> implements Serializable {

    private static final long serialVersionUID = 1L;

    protected ID id;

    protected Entity(){
        super();
    }

    public Entity(ID id) {
        requireNotNull("Id cannot null", id);
        this.id = id;
    }

    public ID getId() {
        return id;
    }
}
