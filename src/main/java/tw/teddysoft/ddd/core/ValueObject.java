package tw.teddysoft.ddd.core;

import java.io.Serializable;

public abstract class ValueObject implements Serializable {

    private static final long serialVersionUID = 1L;

    public ValueObject() {
        super();
    }
}
