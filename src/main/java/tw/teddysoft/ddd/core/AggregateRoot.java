package tw.teddysoft.ddd.core;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class AggregateRoot<ID> extends Entity<ID> {

    protected boolean isDeleted;
    private final List<DomainEvent> domainEvents;

    public AggregateRoot(List<DomainEvent> events) {
        super();
        isDeleted = false;
        this.domainEvents = new CopyOnWriteArrayList<>();

    }

    protected AggregateRoot() {
        super();
        isDeleted = false;
        this.domainEvents = new CopyOnWriteArrayList<>();
    }

    public AggregateRoot(ID id) {
        super(id);
        isDeleted = false;
        this.domainEvents = new CopyOnWriteArrayList<>();
    }

    public void addDomainEvent(DomainEvent domainEvent) {
        domainEvents.add(domainEvent);
    }

    public List<DomainEvent> getDomainEvents() {
        return Collections.unmodifiableList(domainEvents);
    }

    public void clearDomainEvents() {
        domainEvents.clear();
    }

    public void apply(DomainEvent event){
        throw new UnsupportedOperationException("Please implement this function in suclasses");
    }

    public abstract void markAsDeleted(String userId);

    public boolean isDeleted(){ return  isDeleted;}

}
