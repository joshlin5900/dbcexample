package tw.teddysoft.ddd.core;

import java.io.Serializable;
import java.time.Instant;
import java.util.UUID;

public class DomainEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	private UUID id;

	private Instant occurredOn;

	public DomainEvent(Instant occurredOn){
		super();
		id = UUID.randomUUID();
		this.occurredOn = occurredOn;
	}

	public DomainEvent(UUID id, Instant occurredOn){
		super();
		this.id = id;
		this.occurredOn = occurredOn;
	}

	public Instant occurredOn() {
		return occurredOn;
	}

	public String detail() {
		String formatDate = String.format("occurredOn='%1$tY-%1$tm-%1$td %1$tH:%1$tM']", occurredOn());
		String format = String.format(
				"%s[Name='%s'] ",
				this.getClass().getSimpleName());
		return format + formatDate;
	}

	public UUID getId() {
		return id;
	}

	public Instant getOccurredOn() {
		return occurredOn;
	}

}
