package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.ValueObject;

import java.util.UUID;

public class BoardId extends ValueObject {

    private final String id;

    public BoardId(String id){
        super();
        this.id = id;
    }

    public BoardId(UUID id){
        super();
        this.id = id.toString();
    }

    public String value() {
        return id;
    }

    public static BoardId valueOf(String id){
        return new BoardId(id);
    }

    public static BoardId create(){
        return new BoardId(UUID.randomUUID().toString());
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof BoardId))
            return false;

        BoardId target = (BoardId) o;
        return target.id.equals(id) ;
    }

    @Override public int hashCode() {
        int result = 17;
        result = 31 * result + id.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return id;
    }
}
