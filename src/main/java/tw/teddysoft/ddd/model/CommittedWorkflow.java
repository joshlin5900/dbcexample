package tw.teddysoft.ddd.model;

import tw.teddysoft.ddd.core.Entity;

public class CommittedWorkflow extends Entity {
    private final BoardId boardId;
    private final WorkflowId workflowId;
    private int order;

    public CommittedWorkflow(BoardId boardId, WorkflowId workflowId, int order) {
        this.boardId = boardId;
        this.workflowId = workflowId;
        this.order = order;
    }

    public BoardId getBoardId() {
        return boardId;
    }

    public WorkflowId getWorkflowId() {
        return workflowId;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

}
